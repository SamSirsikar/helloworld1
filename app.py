from flask import Flask
import os

app = Flask(__name__)

@app.route("/app1")
def hello():
    return "Here we are at App1 homepage! Hello"

if __name__ == "__main__":
    host = os.getenv('IP', '0.0.0.0')
    port = os.getenv('PORT', 8080)
    app.run(host=host,port=int(port))
